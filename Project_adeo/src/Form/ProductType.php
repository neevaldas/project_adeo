<?php

namespace App\Form;

use App\Entity\Admin;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('SKU', IntegerType::class)
            ->add('status', ChoiceType::class, [
                'label' => 'Status',
                'choices' => [
                    'Enabled' => true,
                    'Disabled' => false
                ]
            ])
            ->add('base_price', MoneyType::class)
            ->add('individual_discount', PercentType::class, [
                'type' => 'integer'
            ])
            ->add('description', TextareaType::class)
            ->add('image', FileType::class, [
                'mapped' => false,
                'label' => 'Upload image',
                'constraints' => [
                    new File([
                        'maxSize' => '10M',
                        'mimeTypes' => [
                            'image/jpg',
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload image in .jpg, .jpeg or .png format',
                    ])
                ],
            ])
            ->add('published_by', EntityType::class, [
                'class' => Admin::class,
                'choice_label' => 'username',
                'disabled' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
