<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReviewRepository")
 */
class Review
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reviewer_name;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="text")
     */
    private $comment;

    /**
     * @Assert\NotBlank
     * @Assert\Range(
     *      min = 1,
     *      max = 5,
     *      minMessage = "Rating must be at least {{ limit }}%",
     *      maxMessage = "Rating cannot be bigger than {{ limit }}%"
     * )
     * @ORM\Column(type="integer")
     */
    private $rating;

    /**
     * @ORM\Column(type="datetime")
     */
    private $post_time;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="reviews")
     * @ORM\JoinColumn(nullable=false)
     */
    private $review_of_product;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReviewerName(): ?string
    {
        return $this->reviewer_name;
    }

    public function setReviewerName(string $reviewer_name): self
    {
        $this->reviewer_name = $reviewer_name;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getPostTime(): ?\DateTimeInterface
    {
        return $this->post_time;
    }

    public function setPostTime(\DateTimeInterface $post_time): self
    {
        $this->post_time = $post_time;

        return $this;
    }

    public function getReviewOfProduct(): ?Product
    {
        return $this->review_of_product;
    }

    public function setReviewOfProduct(?Product $review_of_product): self
    {
        $this->review_of_product = $review_of_product;

        return $this;
    }
}
