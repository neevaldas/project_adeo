<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationRepository")
 */
class Configuration
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Type("integer")
     * @Assert\Positive
     * @ORM\Column(type="integer")
     */
    private $tax_rate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_tax_included;

    /**
     * * @Assert\Range(
     *      min = 1,
     *      max = 100,
     *      minMessage = "Discount must be at least {{ limit }}%",
     *      maxMessage = "Discount cannot be bigger than {{ limit }}%"
     * )
     * @Assert\Type("integer")
     * @Assert\Positive
     * @ORM\Column(type="integer", nullable=true)
     */
    private $global_discount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTaxRate(): ?int
    {
        return $this->tax_rate;
    }

    public function setTaxRate(int $tax_rate): self
    {
        $this->tax_rate = $tax_rate;

        return $this;
    }

    public function getIsTaxIncluded(): ?bool
    {
        return $this->is_tax_included;
    }

    public function setIsTaxIncluded(bool $is_tax_included): self
    {
        $this->is_tax_included = $is_tax_included;

        return $this;
    }

    public function getGlobalDiscount(): ?int
    {
        return $this->global_discount;
    }

    public function setGlobalDiscount(?int $global_discount): self
    {
        $this->global_discount = $global_discount;

        return $this;
    }
}
