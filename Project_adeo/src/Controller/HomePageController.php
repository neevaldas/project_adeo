<?php

namespace App\Controller;

use App\Entity\Configuration;
use App\Entity\Review;
use App\Form\ReviewType;
use App\Repository\ConfigurationRepository;
use PhpParser\Parser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

class HomePageController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index(Request $request, ProductRepository $productRepository, ConfigurationRepository $configRepo, PaginatorInterface $paginator): Response
    {
        $pagination = $paginator->paginate(
            $productRepository->findBy(['status' => true]),
            $request->query->getInt('page', 1), /*page number*/
            16 /*limit per page*/
        );

        return $this->render('home_page/index.html.twig', [
            'controller_name' => 'HomePageController',
            'products' => $pagination,
            'configuration' => $configRepo->findAll()[0]
        ]);
    }

    /**
     * @Route("/product/{id}", name="product_show", methods={"GET","POST"})
     */
    public function show(Product $product, Request $request, ConfigurationRepository $configRepo): Response
    {
        $review = new Review();
        $form = $this->createForm(ReviewType::class, $review);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $now = new \DateTime();
            $now->format('Y-m-d H:i:s');

            $review->setPostTime($now);
            $review->setReviewOfProduct($product);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($review);
            $entityManager->flush();

            return $this->redirectToRoute('product_show', [
                'id' => $product->getId()
            ]);
        }

        $reviews = $product->getReviews()->toArray();
        rsort($reviews);

        return $this->render('home_page/showProduct.html.twig', [
            'product' => $product,
            'reviews' => $reviews,
            'form' => $form->createView(),
            'configuration' => $configRepo->findAll()[0]
        ]);
    }
}
