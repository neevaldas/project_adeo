<?php

namespace App\Controller;

use App\Entity\Configuration;
use App\Form\ConfigurationType;
use App\Repository\ConfigurationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/configuration")
 */
class ConfigurationController extends AbstractController
{
    /**
     * @Route("/", name="configuration_index", methods={"GET"})
     */
    public function index(ConfigurationRepository $configurationRepository): Response
    {
        return $this->render('configuration/index.html.twig', [
            'configurations' => $configurationRepository->findAll(),
            'message' => ''
        ]);
    }

    /**
     * @Route("/{id}/edit", name="configuration_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Configuration $configuration): Response
    {
        $form = $this->createForm(ConfigurationType::class, $configuration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('configuration_index');
        }

        return $this->render('configuration/edit.html.twig', [
            'configuration' => $configuration,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/default_tax_rate", name="default_tax_rate", methods={"GET","POST"})
     * @param Configuration $configuration
     * @return Response
     */
    public  function setDefaultTaxRate(Configuration $configuration): Response
    {
        $default_tax_rate = 21;

        if($configuration->getTaxRate() != $default_tax_rate)
        {
            $configuration->setTaxRate($default_tax_rate);
            $this->getDoctrine()->getManager()->flush();

        }

        return $this->redirectToRoute('configuration_index');
    }
}
