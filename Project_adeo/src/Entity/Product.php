<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @UniqueEntity("SKU")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\NotBlank
     * @Assert\Type("integer")
     * @Assert\Positive
     * @ORM\Column(type="integer", unique=true)
     */
    private $SKU;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @Assert\NotBlank
     * @Assert\Positive
     * @ORM\Column(type="float")
     */
    private $base_price;

    /**
     * @Assert\Range(
     *      min = 1,
     *      max = 100,
     *      minMessage = "Discount must be at least {{ limit }}%",
     *      maxMessage = "Discount cannot be bigger than {{ limit }}%"
     * )
     * @ORM\Column(type="integer", nullable=true)
     */
    private $individual_discount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin", inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $published_by;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Review", mappedBy="review_of_product")
     */
    private $reviews;

    public function __construct()
    {
        $this->reviews = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSKU(): ?int
    {
        return $this->SKU;
    }

    public function setSKU(int $SKU): self
    {
        $this->SKU = $SKU;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getBasePrice(): ?float
    {
        return $this->base_price;
    }

    public function setBasePrice(float $base_price): self
    {
        $this->base_price = $base_price;

        return $this;
    }

    public function getIndividualDiscount(): ?int
    {
        return $this->individual_discount;
    }

    public function setIndividualDiscount(?int $individual_discount): self
    {
        $this->individual_discount = $individual_discount;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPublishedBy(): ?Admin
    {
        return $this->published_by;
    }

    public function setPublishedBy(?Admin $published_by): self
    {
        $this->published_by = $published_by;

        return $this;
    }

    /**
     * @return Collection|Review[]
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setReviewOfProduct($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->contains($review)) {
            $this->reviews->removeElement($review);
            // set the owning side to null (unless already changed)
            if ($review->getReviewOfProduct() === $this) {
                $review->setReviewOfProduct(null);
            }
        }

        return $this;
    }
}
